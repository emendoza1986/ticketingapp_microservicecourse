import {Publisher, Subjects, TicketCreatedEvent} from "@tickets.dev01/common";

export class TicketCreatedPublisher extends Publisher<TicketCreatedEvent>{
    readonly subject = Subjects.TicketCreated;

}