import express from 'express';

import {routes} from "../routes";
import {currentUser} from "@tickets.dev01/common";


const router =express.Router()

router.get(routes.currentUser, currentUser, (req,res) =>{
    res.send({currentUser: req.currentUser || null});
})

export { router as currentUserRouter}