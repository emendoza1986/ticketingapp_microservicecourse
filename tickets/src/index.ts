import 'express-async-errors';
import mongoose from 'mongoose';

import {endpoints} from './endpoints';
import {app} from './app';
import {natsWrapper} from "./nats-wrapper"; //sharing instance


const startApp = async () => {
    if (!process.env.JWT_KEY) {
        throw new Error('JWT_KEY must be defined');
    }
    if (!process.env.NATS_CLIENT_ID) {
        throw new Error('NATS_CLIENT_ID must be defined');
    }

    try {
        await natsWrapper.connect(endpoints.nats_cluster_id, process.env.NATS_CLIENT_ID, endpoints.nats_srv);
        natsWrapper.client.on('close', () => {
            console.log('NATS connection closed!');
            process.exit();
        })

        process.on('SIGINT', () => natsWrapper.client.close());
        process.on('SIGTERM', () => natsWrapper.client.close());


        await mongoose.connect(endpoints.tickets_mongo_srv, { //todo
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        });
        console.log('Connected to MongoDb');
    } catch (err) {
        console.error(err);
    }

    app.listen(endpoints.tickets_srv_port, () => {//todo
        console.log(endpoints.tickets_srv);//todo
    });
};

startApp();
