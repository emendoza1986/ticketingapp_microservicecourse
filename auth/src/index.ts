import 'express-async-errors';
import mongoose from 'mongoose';

import {endpoints} from './endpoints';
import {app} from './app';



const startApp = async () => {
  if (!process.env.JWT_KEY) {
    throw new Error('JWT_KEY must be defined');
  }

  try {
    await mongoose.connect(endpoints.auth_mongo_srv, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true
    });
    console.log('Connected to MongoDb');
  } catch (err) {
    console.error(err);
  }

  app.listen(endpoints.auth_srv_port, () => {
    console.log(endpoints.auth_srv);
  });
};

startApp();
