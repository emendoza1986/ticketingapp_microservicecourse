import express, {Request, Response} from "express";
import {body} from 'express-validator';
import {NotFoundError , validateRequest} from '@tickets.dev01/common';

import { Ticket} from "../models/ticket";
import {routes} from "../routes";

const router = express.Router();

router.get(routes.ticketsApi, async (req: Request, res: Response) =>{
    const tickets = await Ticket.find({})
    res.send(tickets);

});

export {router as indexTicketRouter}