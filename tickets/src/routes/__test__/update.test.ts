import request from 'supertest';
import {app} from '../../app';
import {routes} from "../../routes";
import { Ticket} from '../../models/ticket';
import mongoose from "mongoose";

it('returns a 404 (not found) if the provided id does not exist', async () => {
    const id = new mongoose.Types.ObjectId().toHexString()
    await request(app)
        .put(routes.ticketsApi +`/${id}`)
        .set('Cookie', global.signin())
        .send({
            title: 'asdf',
            price: 20
        })
        .expect(404);
});

it('returns a 401 (forbidden) if the user is not authenticated', async () => {
    const id = new mongoose.Types.ObjectId().toHexString()
    await request(app)
        .put(routes.ticketsApi +`/${id}`)
        .send({
            title: 'asdf',
            price: 20
        })
        .expect(401);
});

it('returns a 401 (forbidden) if the user does not own the ticket', async () => {
    const response = await request(app)
        .post(routes.ticketsApi)
        .set('Cookie', global.signin())
        .send({
            title: 'asdf',
            price: 20
        });

    await request(app)
        .put(routes.ticketsApi + `/${response.body.id}`)
        .set('Cookie', global.signin())
        .send({
            title: 'asdf1234',
            price: 20.99
        })
        .expect(401);
});

it('returns a 400 (invalid) if the user provides an invalid title or price', async () => {
    const cookie = global.signin();

    const response = await request(app)
        .post(routes.ticketsApi)
        .set('Cookie', cookie)
        .send({
            title: 'asdf',
            price: 20
        });

    await request(app)
        .put(routes.ticketsApi + `/${response.body.id}`)
        .set('Cookie', cookie)
        .send({
            title: '',
            price: 20
        })
        .expect(400);

    await request(app)
        .put(routes.ticketsApi + `/${response.body.id}`)
        .set('Cookie', cookie)
        .send({
            title: 'asdf',
            price: -1
        })
        .expect(400);
});

it('updates ticket provided valid inputs', async () => {
    const cookie = global.signin();

    const response = await request(app)
        .post(routes.ticketsApi)
        .set('Cookie', cookie)
        .send({
            title: 'asdf',
            price: 20
        });

    await request(app)
        .put(routes.ticketsApi + `/${response.body.id}`)
        .set('Cookie', cookie)
        .send({
            title: 'asdf1234',
            price: 20.99
        })
        .expect(200);

    const ticketResponse = await request(app)
        .get(routes.ticketsApi + `/${response.body.id}`)
        .send()

    expect(ticketResponse.body.title).toEqual('asdf1234')
    expect(ticketResponse.body.price).toEqual(20.99)
});