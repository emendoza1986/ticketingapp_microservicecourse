import {OrderCreatedEvent, Publisher, Subjects} from "@tickets.dev01/common";

export class OrderCreatedPublisher extends Publisher<OrderCreatedEvent>{
    subject: Subjects.OrderCreated = Subjects.OrderCreated;
}