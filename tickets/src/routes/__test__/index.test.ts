import request from 'supertest';
import {app} from '../../app';
import {routes} from "../../routes";

const createTicket = () => {
    return request(app)
        .post(routes.ticketsApi)
        .set('Cookie', global.signin())
        .send({
            title: 'asdf',
            price: 9.99
        });
}

it('returns a 404 if the ticket is not found', async () => {
    await createTicket();
    await createTicket();
    await createTicket();

    const response = await request(app)
        .get(routes.ticketsApi)
        .send()
        .expect(200);
    expect(response.body.length).toEqual(3)
});