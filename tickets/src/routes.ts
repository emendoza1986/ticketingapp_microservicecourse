export const routes = {
    version: 1,
    ticketsApi: '/api/tickets',
    signUP: '/api/users/signup',
    signOUT: '/api/users/signout',
    signIN: '/api/users/signin',
    currentUser: '/api/users/currentuser',
}
