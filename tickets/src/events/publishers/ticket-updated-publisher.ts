import {Publisher, Subjects, TicketUpdatedEvent} from "@tickets.dev01/common";

export class TicketUpdatedPublisher extends Publisher<TicketUpdatedEvent>{
    readonly subject = Subjects.TicketUpdated;

}