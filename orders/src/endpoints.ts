export const endpoints = {//todo
    version: 1,
    orders_srv_port: 3000,
    orders_srv: 'https://orders-srv:3000',
    orders_mongo_srv_port: 27017,
    orders_mongo_srv: 'mongodb://orders-mongo-srv:27017/orders',
    nats_srv: 'http://nats-srv:4222',
    nats_cluster_id: 'ticketing'
}

