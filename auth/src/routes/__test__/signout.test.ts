import request from 'supertest';
import {app} from '../../app';
import {routes} from "../../routes";


it('clears cookie after signing OUT', async () =>{
    const response = await request(app)
        .post(routes.signUP)
        .send({
            email: 'test@test.com',
            password: 'password'
        })
        .expect(201)


    expect(response.get('Set-Cookie')[0]).toBeDefined()


    const response2 = await request(app)
        .post(routes.signOUT)
        .send({
        })
        .expect(200)


    expect(response2.get('Set-Cookie')[0]).toBe('express:sess=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT; httponly')
})