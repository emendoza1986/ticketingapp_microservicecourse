import request from 'supertest';
import {app} from '../../app';
import {routes} from "../../routes";
import { Ticket} from '../../models/ticket';


it('it has a route handler listening to /api/tickets for post requests', async () => {
    const response = await request(app)
        .post(routes.ticketsApi)
        .send({});
    expect(response.status).not.toEqual(404);
});

it('it can only be accessed if user is signed in', async () => {
    const response = await request(app)
        .post(routes.ticketsApi)
        .send({})
        .expect(401) //not authenticated

});

it('returns a status other than 401 if user signed in', async () => {
    const response = await request(app)
        .post(routes.ticketsApi)
        .set('Cookie', global.signin())
        .send({});

    expect(response.status).not.toEqual(401);

});

it('it returns an error if an invalid title is provided', async () => {
    await request(app)
        .post(routes.ticketsApi)
        .set('Cookie', global.signin())
        .send({
            title: '',
            price: 10.10
        })
        .expect(400)

    await request(app)
        .post(routes.ticketsApi)
        .set('Cookie', global.signin())
        .send({
            price: 10.10
        })
        .expect(400)
});

it('it returns an error if an invalid price is provided', async () => {
    await request(app)
        .post(routes.ticketsApi)
        .set('Cookie', global.signin())
        .send({
            title: 'Apple Jams',
            price: -10
        })
        .expect(400)

    await request(app)
        .post(routes.ticketsApi)
        .set('Cookie', global.signin())
        .send({
            title: 'Apple Jams',

        })
        .expect(400)
});

it('it creates a ticket with valid input', async () => {
    let tickets = await Ticket.find({});
    expect(tickets.length).toEqual(0);

    await request(app)
        .post(routes.ticketsApi)
        .set('Cookie', global.signin())
        .send({
            title: 'Apple Jams',
            price: 10.99
        })
        .expect(201)

    tickets = await Ticket.find({});
    expect(tickets.length).toEqual(1);
});