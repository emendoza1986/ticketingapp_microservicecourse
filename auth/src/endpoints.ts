export const endpoints = {
    version: 1,
    auth_srv_port: 3000,
    auth_srv: 'https://auth-srv:3000',
    auth_mongo_srv_port: 27017,
    auth_mongo_srv: 'mongodb://auth-mongo-srv:27017/auth',
}

