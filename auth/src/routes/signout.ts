import express from 'express'
import cookieSession from 'cookie-session';

import {routes} from "../routes";

const router =express.Router()

router.post(routes.signOUT,(req,res) =>{
    req.session = null;
    res.send('Signed Out');
})

export { router as signOutRouter}