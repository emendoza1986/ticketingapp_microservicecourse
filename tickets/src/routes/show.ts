import express, {Request, Response} from "express";
import {NotFoundError , validateRequest} from '@tickets.dev01/common';

import { Ticket} from "../models/ticket";
import {routes} from "../routes";

const router = express.Router();

router.get(routes.ticketsApi+'/:id', async (req: Request, res: Response) =>{
    const ticket = await Ticket.findById(req.params.id)
    if(!ticket){
        throw new NotFoundError();
    }
    res.send(ticket);


});

export {router as showTicketRouter}