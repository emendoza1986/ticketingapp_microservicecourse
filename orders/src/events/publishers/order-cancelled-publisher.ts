import {OrderCancelledEvent, Publisher, Subjects} from "@tickets.dev01/common";

export class OrderCancelledPublisher extends Publisher<OrderCancelledEvent>{
    subject: Subjects.OrderCancelled = Subjects.OrderCancelled;
}