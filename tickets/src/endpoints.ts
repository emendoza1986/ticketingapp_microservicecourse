export const endpoints = {//todo
    version: 1,
    tickets_srv_port: 3000,
    tickets_srv: 'https://tickets-srv:3000',
    tickets_mongo_srv_port: 27017,
    tickets_mongo_srv: 'mongodb://tickets-mongo-srv:27017/tickets',
    nats_srv: 'http://nats-srv:4222',
    nats_cluster_id: 'ticketing'
}

