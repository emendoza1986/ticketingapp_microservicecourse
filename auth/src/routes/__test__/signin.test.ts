import request from 'supertest';
import {app} from '../../app';
import {routes} from "../../routes";

it('returns 400 account does NOT exist', async () =>{
    return request(app)
        .post(routes.signIN)
        .send({
            email: 'test@test.com',
            password: 'password'
        })
        .expect(400)
})

it('returns 400 when incorrect password', async () =>{
    await request(app)
        .post(routes.signUP)
        .send({
            email: 'test@test.com',
            password: 'password'
        })
        .expect(201)

    await request(app)
        .post(routes.signIN)
        .send({
            email: 'test@test.com',
            password: 'PaSsWoRd'
        })
        .expect(400)
})

it('responds with cookie with valid credentials', async () =>{
    await request(app)
        .post(routes.signUP)
        .send({
            email: 'test@test.com',
            password: 'password'
        })
        .expect(201)

    const response = await request(app)
        .post(routes.signIN)
        .send({
            email: 'test@test.com',
            password: 'password'
        })
        .expect(200)

    expect(response.get('Set-Cookie')).toBeDefined()
})