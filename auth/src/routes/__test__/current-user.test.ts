import request from 'supertest';
import {app} from '../../app';
import {routes} from "../../routes";

it('responds with details about the current user', async () =>{
    const testEmail = 'test@test.com'

    const cookie = await global.signin();

    const response = await request(app)
        .get(routes.currentUser)
        .set('Cookie', cookie)
        .send()
        .expect(200);

    expect(response.body.currentUser.email).toEqual(testEmail);
})

it('responds with null if not authenticated', async () =>{

    const response = await request(app)
        .get(routes.currentUser)
        .send()
        .expect(200);

    expect(response.body.currentUser).toEqual(null);
})